/**
 * This file is part of A6MTB.
 * Copyright (c) 2018 Patrik Gajdoš <idvert3x@gmail.com>
 *
 * A6MTB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * A6MTB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with A6MTB. If not, see <http://www.gnu.org/licenses/>
 */

// Optimized for Samsung 30Q cells, a bit on the conservative side.
// If you use different cells, you may want to adjust these values.

#define BAT_LOW  ADC_30 // When do we start ramping down
#define BAT_CRIT ADC_25 // When do we shut the light off
PROGMEM const uint8_t voltage_blinks[] = {
            // 0 blinks for less than 1%
    ADC_27, // 1 blink  for 1%-10%
    ADC_32, // 2 blinks for 10-25%
    ADC_35, // 3 blinks for 25%-50%
    ADC_38, // 4 blinks for 50%-75%
    ADC_40, // 5 blinks for 75%-100%
    ADC_42, // 6 blinks for >100%
    255,    // Ceiling, don't remove (7 blinks means "error")
};

inline uint8_t get_battery_blinks() {
    // Return an int, number of "blinks", for approximate battery charge
    // Uses the table above for return values
    uint8_t i, voltage;
    voltage = get_voltage();
    // figure out how many times to blink
    for (i=0; voltage > pgm_read_byte(voltage_blinks + i); i++) {}
    return i;
}
