/**
 * This file is part of A6MTB.
 * Copyright (c) 2018 Patrik Gajdoš <idvert3x@gmail.com>
 *
 * A6MTB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * A6MTB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with A6MTB. If not, see <http://www.gnu.org/licenses/>
 */

#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/sleep.h>
#include "./attiny.h"
#include "./delay.h"
#include "./adc.h"
#include "./battery.h"

// Ignore a spurious warning, the cast was done on purpose
#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"

// State variables
uint8_t eepos = 0;
uint8_t active_mode = 0;

/*
 * This light uses a FET+1 style driver, with a FET on the main PWM channel
 * for the brightest high modes and a single 7135 chip on the secondary PWM
 * channel so we can get stable, efficient low / medium modes.
 */
#define NUM_MODES 4
PROGMEM const uint8_t modes_pwm[] = { FAST, FAST, FAST, PHASE };
PROGMEM const uint8_t modes_lgc[] = { 0   , 0   , 90  , 255   }; // PWM levels for the big circuit ( FET )
PROGMEM const uint8_t modes_smc[] = { 50  , 230 , 255 , 0     }; // PWM levels for the small circuit ( 7135 )

// central method for writing (with wear leveling)
void save_mode() {
    uint8_t eep;
    uint8_t oldpos = eepos;

    eepos = (eepos + 1) & (EEPSIZE - 1); // wear leveling, use next cell

    eep = active_mode;
    eeprom_write_byte((uint8_t *)(eepos), eep);   // save current state
    eeprom_write_byte((uint8_t *)(oldpos), 0xff); // erase old state
}

void restore_mode() {
    uint8_t eep;
    // find the data
    for (eepos = 0; eepos < EEPSIZE; eepos++) {
        eep = eeprom_read_byte((const uint8_t *) eepos);
        if (eep != 0xff) {
            active_mode = eep;
            break;
        }
    }
}

void set_output(uint8_t pwm1, uint8_t pwm2) {
    // Need PHASE to properly turn off the light
    if ((pwm1==0) && (pwm2==0)) {
        TCCR0A = PHASE;
    }
    PWM_LVL = pwm1;
    ALT_PWM_LVL = pwm2;
}

void update_mode_outputs() {
    TCCR0A = pgm_read_byte(modes_pwm + active_mode);
    set_output(pgm_read_byte(modes_lgc + active_mode), pgm_read_byte(modes_smc + active_mode));
}

void blink(uint8_t cnt)
{
    while(cnt-- > 0) {
        set_output(0,50);
        _delay_ms(100);
        set_output(0,0);
        _delay_ms(400);
    }
}

inline uint8_t get_cap_val() {
    // Start up ADC for capacitor pin
    DIDR0 |= (1 << CAP_DIDR);                           // disable digital input on ADC pin to reduce power consumption
    ADMUX  = (1 << V_REF) | (1 << ADLAR) | CAP_CHANNEL; // 1.1v reference, left-adjust, ADC3/PB3
    ADCSRA = (1 << ADEN) | (1 << ADSC) | ADC_PRSCL;     // enable, start, prescale

    // Wait for completion
    while (ADCSRA & (1 << ADSC));
    // Start again as datasheet says first result is unreliable
    ADCSRA |= (1 << ADSC);
    // Wait for completion
    while (ADCSRA & (1 << ADSC));

    return ADCH;
}

int main(void) {
    // Read the off-time cap *first* to get the most accurate reading
    uint8_t cap_val = get_cap_val(); // save this for later

    // Set capacitor & PWM pins to output
    DDRB |= (1 << CAP_PIN) | (1 << PWM_PIN) | (1 << ALT_PWM_PIN);

    // Charge up the capacitor
    PORTB |= (1 << CAP_PIN); // High

    TCCR0A = PHASE; // phase-correct PWM both channels
    TCCR0B = 0x01;  // set prescaler to 1

    ADC_on();

    // Load last used mode
    restore_mode();

    if (cap_val > CAP_SHORT) {
        // User did a short press, go to the next mode
        active_mode++;
        if (active_mode >= NUM_MODES) {
            active_mode = 0;
        }
        save_mode();
    } else if (cap_val > CAP_MED) {
        // User did a medium press, blink out the battery status
        _delay_s();
        blink(get_battery_blinks());
        _delay_s();
    }

    update_mode_outputs();

    uint8_t lowbatt_cnt = 0;
    uint8_t voltage;

    for (;;) {
        voltage = get_voltage();

        if (voltage < BAT_CRIT) {
            set_output(0, 0);                    // LED off
            PORTB &= ~(1 << CAP_PIN);            // Cap off
            set_sleep_mode(SLEEP_MODE_PWR_DOWN); // MCU lowest power consumption mode
            sleep_mode();
        }

        if (voltage < BAT_LOW) {
            lowbatt_cnt++;
        } else {
            lowbatt_cnt = 0;
        }

        // See if the voltage has been low for 3 seconds
        if (lowbatt_cnt >= 6) {
            if (active_mode > 0) { // If we are not at the lowest level,
                active_mode -= 1;  // step down one level at a time
                save_mode();
                update_mode_outputs();
            }
            lowbatt_cnt = 0;
        }

        _delay_ms(500);
    }
}
