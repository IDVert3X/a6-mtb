MCU        = attiny13
PARTNO     = t13
FUSES      = -Ulfuse:w:0x75:m -Uhfuse:w:0xfd:m
PROGRAMMER = usbasp

PROGRAM = a6_mtb
CC      = avr-gcc -Wall -Os -mmcu=$(MCU)
AVRDUDE = avrdude -c $(PROGRAMMER) -p $(PARTNO)
OBJS    = $(PROGRAM).o
Q := @

all: $(PROGRAM).hex

%.o: %.c
	@printf "  CC      $(subst $(shell pwd)/,,$(@))\n"
	$(Q)$(CC) -o $@ -c $<

$(PROGRAM).elf: $(PROGRAM).o
	@printf "  LD      $(subst $(shell pwd)/,,$(@))\n"
	$(Q)$(CC) -o $@ $^
	@printf "  SIZE    $(subst $(shell pwd)/,,$(@))\n"
	$(Q)avr-size --format=avr --mcu=$(MCU) $@

$(PROGRAM).hex: $(PROGRAM).elf
	@printf "  OBJCOPY $(subst $(shell pwd)/,,$(@))\n"
	$(Q)avr-objcopy -O ihex $< $@

flash: $(PROGRAM).hex
	@printf "  FLASH   $(PROGRAM).hex\n"
	$(Q)$(AVRDUDE) -u -Uflash:w:$(PROGRAM).hex $(FUSES)

clean:
	@printf "  CLEAN   $(subst $(shell pwd)/,,$(OBJS))\n"
	$(Q)rm -f $(OBJS)
	@printf "  CLEAN   $(PROGRAM).elf\n"
	$(Q)rm -f *.elf
	@printf "  CLEAN   $(PROGRAM).hex\n"
	$(Q)rm -f $(PROGRAM).hex
