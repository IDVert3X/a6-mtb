# A6MTB

Custom firmware for the BLF A6 flashlight, optimized for mountain biking.

The primary focus of this firmware is to provide the rider with very simple to 
operate UI, no max power restrictions and better battery management.

For step-by-step disassembly & firmware flashing guide, tips for mounting the
flashlight onto a bike helmet and other useful information, visit
[project's wiki](https://gitlab.com/IDVert3X/a6-mtb/wikis/home).

### Main features:
- Smart battery management that will prolong the runtime at low battery levels
- 4 modes: dipped beam - low - mid - max
- Easily accessible battery check
- No time limit on MAX power
- Memory ( the flashlight will remember the last used mode )
- No hidden modes or anything that you could unwillingly get into

### Battery management
When the battery reaches 3V, the flashlight will try to keep the battery above 
that voltage, prolonging the usable battery life. This is done by switching to 
a lower mode after the voltage has been low for at least 3s. You can always
switch to a higher mode for a short period of time in case of an emergency.
If the voltage keeps dropping further but there is no lower mode to switch to, 
the flashlight will stay in the lowest mode till it reaches a critical threshold 
of 2.5V, after which the flashlight will switch itself off.

### Modes
Short press of a button will cycle through the modes.
Dipped beam mode lowers the power output to a tolerable level that will not 
blind the oncoming traffic.
The use of max power should be limited to high-speed passages so the light has 
enough airflow to keep it cool.

### Battery check
Medium press ( ~0.5s ) will blink out the remaining capacity.
```
 empty  - no blinks
 1~10%  - 1 blink
10~25%  - 2 blinks
25~50%  - 3 blinks
50~75%  - 4 blinks
75~100% - 5 blinks
> 100%  - 6 blinks
```
By default, these readings are optimized for Samsung 30Q cells.
You can easily change the thresholds in the [battery.h file](./battery.h) 
to better reflect your cell's remaining capacity.  
You can also add more levels if you want to.

### Compilation / flashing ( Linux )
[Makefile](./Makefile) is included and configured for the USBasp programmer.  
If you use a different programmer, change the `PROGRAMMER` variable accordingly.

To build the firmware, simply run `make`.  
To clean the built files, use `make clean`.  
To flash the firmware and fuses, run `make flash`.  

Note: 
- the `flash` command will automatically build the firmware from the source if 
the binary file (a6_mtb.hex) is not present, thus no need to run `make` before.
- when you make a change in the code and want rebuild the firmware, you need to 
run the `clean` command first. Otherwise, you will receive `Nothing to be done`
message, indicating that the firmware has already been built.

Packages needed to build & flash the firmware: 
`build-essential` `gcc-avr` `binutils-avr` `avr-libc` `avrdude`

### Credits
This project uses some code from the original firmware written by *Selene 
"ToyKeeper" Scriven*. Original source code can be found 
[here](https://code.launchpad.net/~toykeeper/flashlight-firmware/blf-a6-final).
