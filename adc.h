/**
 * This file is part of A6MTB.
 * Copyright (c) 2018 Patrik Gajdoš <idvert3x@gmail.com>
 *
 * A6MTB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * A6MTB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with A6MTB. If not, see <http://www.gnu.org/licenses/>
 */

#define ADC_44  183
#define ADC_43  178
#define ADC_42  174
#define ADC_41  170
#define ADC_40  166
#define ADC_39  162
#define ADC_38  158
#define ADC_37  154
#define ADC_36  149
#define ADC_35  145
#define ADC_34  141
#define ADC_33  137
#define ADC_32  133
#define ADC_31  129
#define ADC_30  125
#define ADC_29  120
#define ADC_28  116
#define ADC_27  112
#define ADC_26  108
#define ADC_25  104
#define ADC_24  100
#define ADC_23  95
#define ADC_22  91
#define ADC_21  87
#define ADC_20  83

/********************** Offtime capacitor calibration ********************/
// Values are between 1 and 255
// These #defines are the edge boundaries, not the center of the target.

// Anything higher than this is a "short press"
#define CAP_SHORT           245
// Between CAP_MED and CAP_SHORT is a "medium press"
#define CAP_MED             180
// Below CAP_MED is a long press

inline void ADC_on() {
    // disable digital input on ADC pin to reduce power consumption
    DIDR0 |= (1 << ADC_DIDR);
    // 1.1v reference, left-adjust, ADC1/PB2
    ADMUX  = (1 << V_REF) | (1 << ADLAR) | ADC_CHANNEL;
    // enable, start, prescale
    ADCSRA = (1 << ADEN ) | (1 << ADSC ) | ADC_PRSCL;
}

uint8_t get_voltage() {
    // Start conversion
    ADCSRA |= (1 << ADSC);
    // Wait for completion
    while (ADCSRA & (1 << ADSC));
    // Send back the result
    return ADCH;
}
