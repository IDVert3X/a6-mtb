/**
 * This file is part of A6MTB.
 * Copyright (c) 2018 Patrik Gajdoš <idvert3x@gmail.com>
 *
 * A6MTB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * A6MTB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with A6MTB. If not, see <http://www.gnu.org/licenses/>
 */

#include <avr/sleep.h>
#include "../attiny.h"
#include "../delay.h"
#include "../adc.h"

void set_output(uint8_t pwm1, uint8_t pwm2) 
{
    if ((pwm1==0) && (pwm2==0)) {
        TCCR0A = PHASE;
    } else {
        TCCR0A = FAST;
    }
    PWM_LVL = pwm1;
    ALT_PWM_LVL = pwm2;
}

void blink()
{
    set_output(0,50);
    _delay_ms(100);
    set_output(0,0);
    _delay_ms(500);
}

void short_blink() 
{
    set_output(0,20);
    _delay_ms(5);
    set_output(0,0);
    _delay_ms(500);
}

int main(void)
{
    DDRB |= ((1 << PWM_PIN) | (1 << ALT_PWM_PIN));
    TCCR0B = 0x01; // pre-scaler for timer 
    ADC_on();

    uint16_t voltage;
    uint8_t i;
    for(;;) {
        // get an average of several readings
        voltage = 0;
        for (i=0; i<8; i++) {
            voltage += get_voltage();
            _delay_ms(50);
        }
        voltage /= 8;

        // hundreds
        while (voltage >= 100) {
            voltage -= 100;
            blink();
        }
        _delay_ms(1000);

        // tens
        if (voltage < 10) {
            short_blink();
        }
        while (voltage >= 10) {
            voltage -= 10;
            blink();
        }
        _delay_ms(1000);

        // ones
        if (voltage <= 0) {
            short_blink();
        }
        while (voltage > 0) {
            voltage -= 1;
            blink();
        }
        
        _delay_ms(3000);
    }
}
