# A6MTB / ADC Calibration

This firmware is intended for ADC calibration.

When the flashlight is turned on, it will blink out the measured ADC value, 
one digit at a time.  
There is a 1 second delay between each digit, dim/short blink means zero.

Examples:   
ADC value of **145**: 
`blink---blink-blink-blink-blink---blink-blink-blink-blink-blink`  
ADC value of **302**: 
`blink-blink-blink---dim blink---blink-blink`

Once you know the ADC value and corresponding battery voltage ( eg. measured
using multimeter ), you can easily calculate & replace the calibration values
in the [adc.h file](../adc.h). It's recommended to take multiple measurements
at different voltage levels and use average of these to improve accuracy.
